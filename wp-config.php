<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yugo-concept' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'd$3a^!rNsAFKZ[c:=^n=)=JDTKIHH*tF?]]R{WM~8.;Qj@z3zh.}TcUtWiC{9*_e' );
define( 'SECURE_AUTH_KEY',  'cn-24yqYh/ce{SV$1|QEDKi.pp`&f@hEJwo5gu v*!`rI H1%DL.JAg +ewB.].v' );
define( 'LOGGED_IN_KEY',    'M_wGhDL?O ]T>,Yxekc%MlD@H3]QBV[HeXZ;BUA[ --`r,[) #>QEia`Ehn6supb' );
define( 'NONCE_KEY',        '([~7];&L>^BcX _j_aT7T_ vSm/XjT@#~iEQps$fqAj^jnVWaY?8($v!ek*7%s0|' );
define( 'AUTH_SALT',        '&B+dATq}sk4Z^=aoqUC5NZf2&hUG]kN}5I}61TD{7/wv&ixIMO<XjUfN[`^ju9n9' );
define( 'SECURE_AUTH_SALT', 'd0*mO_RS/|,dQSYfSJ!i*W1nq m2jfg6#UA&-L hWm,rijy=T6;Mpi54:gK*UTZ%' );
define( 'LOGGED_IN_SALT',   ']6t{o qAX^[RAwe085=Z<9]d@8Hbg<P>iDdY8iDx(]+unGa{{fP8|2*rImM3;s2&' );
define( 'NONCE_SALT',       'V,WtxPS)>VGz^^%h93%fI~3h1pn?aYBd0I6*h3ajh^rq4wZgr3SgBEcedL/NJl{3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
